import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { LouiesAlterierTestModule } from '../../../test.module';
import { AdditionDetailComponent } from 'app/entities/addition/addition-detail.component';
import { Addition } from 'app/shared/model/addition.model';

describe('Component Tests', () => {
  describe('Addition Management Detail Component', () => {
    let comp: AdditionDetailComponent;
    let fixture: ComponentFixture<AdditionDetailComponent>;
    const route = ({ data: of({ addition: new Addition(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [LouiesAlterierTestModule],
        declarations: [AdditionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AdditionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AdditionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load addition on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.addition).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
