import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { LouiesAlterierTestModule } from '../../../test.module';
import { AdditionUpdateComponent } from 'app/entities/addition/addition-update.component';
import { AdditionService } from 'app/entities/addition/addition.service';
import { Addition } from 'app/shared/model/addition.model';

describe('Component Tests', () => {
  describe('Addition Management Update Component', () => {
    let comp: AdditionUpdateComponent;
    let fixture: ComponentFixture<AdditionUpdateComponent>;
    let service: AdditionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [LouiesAlterierTestModule],
        declarations: [AdditionUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(AdditionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AdditionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AdditionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Addition(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Addition();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
