package pl.louiesatelier.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import pl.louiesatelier.web.rest.TestUtil;

public class AdditionDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdditionDTO.class);
        AdditionDTO additionDTO1 = new AdditionDTO();
        additionDTO1.setId(1L);
        AdditionDTO additionDTO2 = new AdditionDTO();
        assertThat(additionDTO1).isNotEqualTo(additionDTO2);
        additionDTO2.setId(additionDTO1.getId());
        assertThat(additionDTO1).isEqualTo(additionDTO2);
        additionDTO2.setId(2L);
        assertThat(additionDTO1).isNotEqualTo(additionDTO2);
        additionDTO1.setId(null);
        assertThat(additionDTO1).isNotEqualTo(additionDTO2);
    }
}
