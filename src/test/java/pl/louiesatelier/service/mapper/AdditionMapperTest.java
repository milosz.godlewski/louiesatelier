package pl.louiesatelier.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AdditionMapperTest {

    private AdditionMapper additionMapper;

    @BeforeEach
    public void setUp() {
        additionMapper = new AdditionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(additionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(additionMapper.fromId(null)).isNull();
    }
}
