package pl.louiesatelier.web.rest;

import pl.louiesatelier.LouiesAlterierApp;
import pl.louiesatelier.domain.Addition;
import pl.louiesatelier.repository.AdditionRepository;
import pl.louiesatelier.service.AdditionService;
import pl.louiesatelier.service.dto.AdditionDTO;
import pl.louiesatelier.service.mapper.AdditionMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AdditionResource} REST controller.
 */
@SpringBootTest(classes = LouiesAlterierApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AdditionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private AdditionRepository additionRepository;

    @Autowired
    private AdditionMapper additionMapper;

    @Autowired
    private AdditionService additionService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAdditionMockMvc;

    private Addition addition;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Addition createEntity(EntityManager em) {
        Addition addition = new Addition()
            .name(DEFAULT_NAME)
            .type(DEFAULT_TYPE);
        return addition;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Addition createUpdatedEntity(EntityManager em) {
        Addition addition = new Addition()
            .name(UPDATED_NAME)
            .type(UPDATED_TYPE);
        return addition;
    }

    @BeforeEach
    public void initTest() {
        addition = createEntity(em);
    }

    @Test
    @Transactional
    public void createAddition() throws Exception {
        int databaseSizeBeforeCreate = additionRepository.findAll().size();
        // Create the Addition
        AdditionDTO additionDTO = additionMapper.toDto(addition);
        restAdditionMockMvc.perform(post("/api/additions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(additionDTO)))
            .andExpect(status().isCreated());

        // Validate the Addition in the database
        List<Addition> additionList = additionRepository.findAll();
        assertThat(additionList).hasSize(databaseSizeBeforeCreate + 1);
        Addition testAddition = additionList.get(additionList.size() - 1);
        assertThat(testAddition.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAddition.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createAdditionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = additionRepository.findAll().size();

        // Create the Addition with an existing ID
        addition.setId(1L);
        AdditionDTO additionDTO = additionMapper.toDto(addition);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdditionMockMvc.perform(post("/api/additions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(additionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Addition in the database
        List<Addition> additionList = additionRepository.findAll();
        assertThat(additionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAdditions() throws Exception {
        // Initialize the database
        additionRepository.saveAndFlush(addition);

        // Get all the additionList
        restAdditionMockMvc.perform(get("/api/additions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(addition.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }
    
    @Test
    @Transactional
    public void getAddition() throws Exception {
        // Initialize the database
        additionRepository.saveAndFlush(addition);

        // Get the addition
        restAdditionMockMvc.perform(get("/api/additions/{id}", addition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(addition.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE));
    }
    @Test
    @Transactional
    public void getNonExistingAddition() throws Exception {
        // Get the addition
        restAdditionMockMvc.perform(get("/api/additions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAddition() throws Exception {
        // Initialize the database
        additionRepository.saveAndFlush(addition);

        int databaseSizeBeforeUpdate = additionRepository.findAll().size();

        // Update the addition
        Addition updatedAddition = additionRepository.findById(addition.getId()).get();
        // Disconnect from session so that the updates on updatedAddition are not directly saved in db
        em.detach(updatedAddition);
        updatedAddition
            .name(UPDATED_NAME)
            .type(UPDATED_TYPE);
        AdditionDTO additionDTO = additionMapper.toDto(updatedAddition);

        restAdditionMockMvc.perform(put("/api/additions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(additionDTO)))
            .andExpect(status().isOk());

        // Validate the Addition in the database
        List<Addition> additionList = additionRepository.findAll();
        assertThat(additionList).hasSize(databaseSizeBeforeUpdate);
        Addition testAddition = additionList.get(additionList.size() - 1);
        assertThat(testAddition.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAddition.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingAddition() throws Exception {
        int databaseSizeBeforeUpdate = additionRepository.findAll().size();

        // Create the Addition
        AdditionDTO additionDTO = additionMapper.toDto(addition);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdditionMockMvc.perform(put("/api/additions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(additionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Addition in the database
        List<Addition> additionList = additionRepository.findAll();
        assertThat(additionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAddition() throws Exception {
        // Initialize the database
        additionRepository.saveAndFlush(addition);

        int databaseSizeBeforeDelete = additionRepository.findAll().size();

        // Delete the addition
        restAdditionMockMvc.perform(delete("/api/additions/{id}", addition.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Addition> additionList = additionRepository.findAll();
        assertThat(additionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
