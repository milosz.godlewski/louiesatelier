package pl.louiesatelier.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import pl.louiesatelier.web.rest.TestUtil;

public class AdditionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Addition.class);
        Addition addition1 = new Addition();
        addition1.setId(1L);
        Addition addition2 = new Addition();
        addition2.setId(addition1.getId());
        assertThat(addition1).isEqualTo(addition2);
        addition2.setId(2L);
        assertThat(addition1).isNotEqualTo(addition2);
        addition1.setId(null);
        assertThat(addition1).isNotEqualTo(addition2);
    }
}
