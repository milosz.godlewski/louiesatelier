package pl.louiesatelier.repository;

import pl.louiesatelier.domain.Addition;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Addition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdditionRepository extends JpaRepository<Addition, Long> {
}
