package pl.louiesatelier.web.rest;

import pl.louiesatelier.service.AdditionService;
import pl.louiesatelier.web.rest.errors.BadRequestAlertException;
import pl.louiesatelier.service.dto.AdditionDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link pl.louiesatelier.domain.Addition}.
 */
@RestController
@RequestMapping("/api")
public class AdditionResource {

    private final Logger log = LoggerFactory.getLogger(AdditionResource.class);

    private static final String ENTITY_NAME = "addition";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AdditionService additionService;

    public AdditionResource(AdditionService additionService) {
        this.additionService = additionService;
    }

    /**
     * {@code POST  /additions} : Create a new addition.
     *
     * @param additionDTO the additionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new additionDTO, or with status {@code 400 (Bad Request)} if the addition has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/additions")
    public ResponseEntity<AdditionDTO> createAddition(@RequestBody AdditionDTO additionDTO) throws URISyntaxException {
        log.debug("REST request to save Addition : {}", additionDTO);
        if (additionDTO.getId() != null) {
            throw new BadRequestAlertException("A new addition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AdditionDTO result = additionService.save(additionDTO);
        return ResponseEntity.created(new URI("/api/additions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /additions} : Updates an existing addition.
     *
     * @param additionDTO the additionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated additionDTO,
     * or with status {@code 400 (Bad Request)} if the additionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the additionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/additions")
    public ResponseEntity<AdditionDTO> updateAddition(@RequestBody AdditionDTO additionDTO) throws URISyntaxException {
        log.debug("REST request to update Addition : {}", additionDTO);
        if (additionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AdditionDTO result = additionService.save(additionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, additionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /additions} : get all the additions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of additions in body.
     */
    @GetMapping("/additions")
    public ResponseEntity<List<AdditionDTO>> getAllAdditions(Pageable pageable) {
        log.debug("REST request to get a page of Additions");
        Page<AdditionDTO> page = additionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /additions/:id} : get the "id" addition.
     *
     * @param id the id of the additionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the additionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/additions/{id}")
    public ResponseEntity<AdditionDTO> getAddition(@PathVariable Long id) {
        log.debug("REST request to get Addition : {}", id);
        Optional<AdditionDTO> additionDTO = additionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(additionDTO);
    }

    /**
     * {@code DELETE  /additions/:id} : delete the "id" addition.
     *
     * @param id the id of the additionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/additions/{id}")
    public ResponseEntity<Void> deleteAddition(@PathVariable Long id) {
        log.debug("REST request to delete Addition : {}", id);
        additionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
