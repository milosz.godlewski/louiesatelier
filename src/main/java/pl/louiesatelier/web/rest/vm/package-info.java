/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.louiesatelier.web.rest.vm;
