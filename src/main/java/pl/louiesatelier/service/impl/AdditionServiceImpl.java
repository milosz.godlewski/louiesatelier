package pl.louiesatelier.service.impl;

import pl.louiesatelier.service.AdditionService;
import pl.louiesatelier.domain.Addition;
import pl.louiesatelier.repository.AdditionRepository;
import pl.louiesatelier.service.dto.AdditionDTO;
import pl.louiesatelier.service.mapper.AdditionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Addition}.
 */
@Service
@Transactional
public class AdditionServiceImpl implements AdditionService {

    private final Logger log = LoggerFactory.getLogger(AdditionServiceImpl.class);

    private final AdditionRepository additionRepository;

    private final AdditionMapper additionMapper;

    public AdditionServiceImpl(AdditionRepository additionRepository, AdditionMapper additionMapper) {
        this.additionRepository = additionRepository;
        this.additionMapper = additionMapper;
    }

    @Override
    public AdditionDTO save(AdditionDTO additionDTO) {
        log.debug("Request to save Addition : {}", additionDTO);
        Addition addition = additionMapper.toEntity(additionDTO);
        addition = additionRepository.save(addition);
        return additionMapper.toDto(addition);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AdditionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Additions");
        return additionRepository.findAll(pageable)
            .map(additionMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<AdditionDTO> findOne(Long id) {
        log.debug("Request to get Addition : {}", id);
        return additionRepository.findById(id)
            .map(additionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Addition : {}", id);
        additionRepository.deleteById(id);
    }
}
