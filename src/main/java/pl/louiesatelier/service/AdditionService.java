package pl.louiesatelier.service;

import pl.louiesatelier.service.dto.AdditionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link pl.louiesatelier.domain.Addition}.
 */
public interface AdditionService {

    /**
     * Save a addition.
     *
     * @param additionDTO the entity to save.
     * @return the persisted entity.
     */
    AdditionDTO save(AdditionDTO additionDTO);

    /**
     * Get all the additions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AdditionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" addition.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AdditionDTO> findOne(Long id);

    /**
     * Delete the "id" addition.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
