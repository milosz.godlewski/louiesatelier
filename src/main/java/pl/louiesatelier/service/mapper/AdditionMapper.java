package pl.louiesatelier.service.mapper;


import pl.louiesatelier.domain.*;
import pl.louiesatelier.service.dto.AdditionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Addition} and its DTO {@link AdditionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AdditionMapper extends EntityMapper<AdditionDTO, Addition> {


    @Mapping(target = "products", ignore = true)
    @Mapping(target = "removeProducts", ignore = true)
    Addition toEntity(AdditionDTO additionDTO);

    default Addition fromId(Long id) {
        if (id == null) {
            return null;
        }
        Addition addition = new Addition();
        addition.setId(id);
        return addition;
    }
}
