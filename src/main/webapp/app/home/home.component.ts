import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;

  constructor(private accountService: AccountService, private loginModalService: LoginModalService) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  csvConverter(csv: any): any {
    const lines = csv.split('\n');
    const result = [];
    for (let i = 0; i < lines.length; i++) {
      const headers = lines[i].split(';');
      const obj = {};
      const currentLine = headers[1].split(',');
      for (let j = 0; j < currentLine.length; j++) {
        obj[j] = currentLine[j];
      }
      result.push(obj);
    }
    return result;
  }

  proccess(): void {
    let products =
      '1;truskawka, czekolada\n2;jabłko, piwonia, bez\n3;cynamon, piżmo, pomarańcza\n4;pomelo, mango, grapefruit; płatki nagietka\n5;konwalia, akacja; zielona herbata\n6;truskawka, mandarynka, Litsea Cubeba\n7;konwalia, mango, brzoskwinia, wanilia, cynamon\n8;lawenda, bergamotka, mięta\n9;golteria, awokado, Niaouli, argan\n10;kawa, czekolada, opium; ziarna kawy\n11;limetka, cytryna, lemongras, mięta; płatki słonecznika\n12;cynamon, goździk, pomarańcza; sól himalajska\n13;sandał, kawa, argan, kamfora, cedr; sól himalajska\n14;frezja, brzoskwinia, kokos, awokado\n15;wiśnia, czekolada; ziarna kawy\n16;róża; płatki róży czerwonej\n17;frezja, fiołek, magnolia; kwiat bławatka\n18;wanilia; sól himalajska\n19;lawenda; susz lawendy\n20;róża, lawenda, rozmaryn; płatki róży czerwonej\n21;sandał, wanilia; sól z Morza Martwego\n22;fiołek, jaśmin; bławatek\n23;jabłko, brzoskwinia, pomelo, grapefruit\n24;fiołek, cytryna, naouli; sól z Morza Martwego\n25;cynamon, Niaouli, anyż, kawa, paczula; sól z Morza Martwego\n26;kokos; sól z Morza Martwego\n27;sandał, wanilia, paczula, awokado; sól himalajska\n28;szałwia, tymianek, drzewo herbaciane; ekstrakt z miłorzębu, hydrolat z czystka\n29;eukaliptus, mięta; sól z Morza Martwego, hydrolat z czystka\n30;cynamon, paczula; sól himalajska, ekstrakt z miłorzębu\n31;aloes, drzewo herbaciane; zielona herbata, hydrolat z czystka, ekstrakt z miłorzębu\n32;cedr, sandał, akacja, bez, jaśmin, paczula, piżmo, pomarańcza';
    let additions =
      '1;akacja\n2;aloes\n3;anyż\n4;argan\n5;awokado\n6;bergamotka\n7;bez\n8;bławatek\n9;brzoskwinia\n10;cedr\n11;cynamon\n12;cytryna\n13;czekolada\n14;drzewo herbaciane\n15;ekstrakt z miłorzębu\n16;eukaliptus\n17;fiołek\n18;frezja\n19;golteria\n20;goździk\n21;grapefruit\n22;hydrolat z czystka\n23;jabłko\n24;jaśmin\n25;kamfora\n26;kawa\n27;kokos\n28;konwalia\n29;kwiat bławatka\n30;lawenda\n31;lemongras\n32;limetka\n33;litsea Cubeba\n34;magnolia\n35;mandarynka\n36;mango\n37;mięta\n38;naouli\n39;niaouli\n40;opium\n41;paczula\n42;piwonia\n43;piżmo\n44;płatki nagietka\n45;płatki róży czerwonej\n46;płatki słonecznika\n47;pomarańcza\n48;pomelo\n49;róża\n50;rozmaryn\n51;sandał\n52;sól himalajska\n53;sól z Morza Martwego\n54;susz lawendy\n55;szałwia\n56;truskawka\n57;tymianek\n58;wanilia\n59;wiśnia\n60;ziarna kawy\n61;zielona herbata\n62;truskawka\n';

    products = this.csvConverter(products);
    additions = this.csvConverter(additions);
    const productsAdditions = {};
    for (let i = 0; i < products.length; i++) {
      for (let j = 0; j < additions.length; j++) {
        if (!products[i][j].indexOf(additions[j])) {
          productsAdditions[i][productsAdditions[i].length];
        }
      }
    }
  }
}
