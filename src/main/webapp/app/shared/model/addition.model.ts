import { IProduct } from 'app/shared/model/product.model';

export interface IAddition {
  id?: number;
  name?: string;
  type?: string;
  products?: IProduct[];
}

export class Addition implements IAddition {
  constructor(public id?: number, public name?: string, public type?: string, public products?: IProduct[]) {}
}
