import { IAddition } from 'app/shared/model/addition.model';

export interface IProduct {
  id?: number;
  name?: string;
  price?: number;
  color?: string;
  imageContentType?: string;
  image?: any;
  category?: string;
  additions?: IAddition[];
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public name?: string,
    public price?: number,
    public color?: string,
    public imageContentType?: string,
    public image?: any,
    public category?: string,
    public additions?: IAddition[]
  ) {}
}
