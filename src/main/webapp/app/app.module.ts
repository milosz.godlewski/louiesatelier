import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { LouiesAlterierSharedModule } from 'app/shared/shared.module';
import { LouiesAlterierCoreModule } from 'app/core/core.module';
import { LouiesAlterierAppRoutingModule } from './app-routing.module';
import { LouiesAlterierHomeModule } from './home/home.module';
import { LouiesAlterierEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    LouiesAlterierSharedModule,
    LouiesAlterierCoreModule,
    LouiesAlterierHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    LouiesAlterierEntityModule,
    LouiesAlterierAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class LouiesAlterierAppModule {}
