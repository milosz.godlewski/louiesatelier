import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAddition } from 'app/shared/model/addition.model';
import { AdditionService } from './addition.service';

@Component({
  templateUrl: './addition-delete-dialog.component.html',
})
export class AdditionDeleteDialogComponent {
  addition?: IAddition;

  constructor(protected additionService: AdditionService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.additionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('additionListModification');
      this.activeModal.close();
    });
  }
}
