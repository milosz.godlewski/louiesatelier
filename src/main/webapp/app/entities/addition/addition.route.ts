import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAddition, Addition } from 'app/shared/model/addition.model';
import { AdditionService } from './addition.service';
import { AdditionComponent } from './addition.component';
import { AdditionDetailComponent } from './addition-detail.component';
import { AdditionUpdateComponent } from './addition-update.component';

@Injectable({ providedIn: 'root' })
export class AdditionResolve implements Resolve<IAddition> {
  constructor(private service: AdditionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAddition> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((addition: HttpResponse<Addition>) => {
          if (addition.body) {
            return of(addition.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Addition());
  }
}

export const additionRoute: Routes = [
  {
    path: '',
    component: AdditionComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'louiesAlterierApp.addition.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AdditionDetailComponent,
    resolve: {
      addition: AdditionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'louiesAlterierApp.addition.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AdditionUpdateComponent,
    resolve: {
      addition: AdditionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'louiesAlterierApp.addition.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AdditionUpdateComponent,
    resolve: {
      addition: AdditionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'louiesAlterierApp.addition.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
