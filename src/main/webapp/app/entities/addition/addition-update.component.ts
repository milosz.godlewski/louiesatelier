import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAddition, Addition } from 'app/shared/model/addition.model';
import { AdditionService } from './addition.service';

@Component({
  selector: 'jhi-addition-update',
  templateUrl: './addition-update.component.html',
})
export class AdditionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    type: [],
  });

  constructor(protected additionService: AdditionService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ addition }) => {
      this.updateForm(addition);
    });
  }

  updateForm(addition: IAddition): void {
    this.editForm.patchValue({
      id: addition.id,
      name: addition.name,
      type: addition.type,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const addition = this.createFromForm();
    if (addition.id !== undefined) {
      this.subscribeToSaveResponse(this.additionService.update(addition));
    } else {
      this.subscribeToSaveResponse(this.additionService.create(addition));
    }
  }

  private createFromForm(): IAddition {
    return {
      ...new Addition(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      type: this.editForm.get(['type'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAddition>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
