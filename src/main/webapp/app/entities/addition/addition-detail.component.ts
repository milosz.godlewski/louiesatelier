import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAddition } from 'app/shared/model/addition.model';

@Component({
  selector: 'jhi-addition-detail',
  templateUrl: './addition-detail.component.html',
})
export class AdditionDetailComponent implements OnInit {
  addition: IAddition | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ addition }) => (this.addition = addition));
  }

  previousState(): void {
    window.history.back();
  }
}
