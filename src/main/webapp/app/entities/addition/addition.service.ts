import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAddition } from 'app/shared/model/addition.model';

type EntityResponseType = HttpResponse<IAddition>;
type EntityArrayResponseType = HttpResponse<IAddition[]>;

@Injectable({ providedIn: 'root' })
export class AdditionService {
  public resourceUrl = SERVER_API_URL + 'api/additions';

  constructor(protected http: HttpClient) {}

  create(addition: IAddition): Observable<EntityResponseType> {
    return this.http.post<IAddition>(this.resourceUrl, addition, { observe: 'response' });
  }

  update(addition: IAddition): Observable<EntityResponseType> {
    return this.http.put<IAddition>(this.resourceUrl, addition, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAddition>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAddition[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
