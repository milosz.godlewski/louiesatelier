import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LouiesAlterierSharedModule } from 'app/shared/shared.module';
import { AdditionComponent } from './addition.component';
import { AdditionDetailComponent } from './addition-detail.component';
import { AdditionUpdateComponent } from './addition-update.component';
import { AdditionDeleteDialogComponent } from './addition-delete-dialog.component';
import { additionRoute } from './addition.route';

@NgModule({
  imports: [LouiesAlterierSharedModule, RouterModule.forChild(additionRoute)],
  declarations: [AdditionComponent, AdditionDetailComponent, AdditionUpdateComponent, AdditionDeleteDialogComponent],
  entryComponents: [AdditionDeleteDialogComponent],
})
export class LouiesAlterierAdditionModule {}
