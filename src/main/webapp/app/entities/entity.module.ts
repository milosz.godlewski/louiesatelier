import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.LouiesAlterierProductModule),
      },
      {
        path: 'addition',
        loadChildren: () => import('./addition/addition.module').then(m => m.LouiesAlterierAdditionModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class LouiesAlterierEntityModule {}
